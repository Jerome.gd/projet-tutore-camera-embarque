Le but de notre projet est de réaliser la visualisation d'une caméra embarqué sur un robot, et de pouvoir commander le robot. 

Nous possédons : 
- un raspbery pi3 b+ 
- la carte moteur est un sarbertooth 2x12 version V1.00 
- 4 moteurs DC
- une batterie ( 12.0V 2800mAh ) pour alimenter les moteurs 
- un robot lynxmotion 